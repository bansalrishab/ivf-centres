**Medical Procedures for Becoming a Surrogate**

It’s common for women who are considering becoming a surrogate to have questions about the medical procedures that are involved for becoming a surrogate mother.

If you choose IARC as your surrogate agency we will walk you though the entire process and will be there to answer questions every step of the way. However, here are some basics on what is medically required to be a surrogate mother.

***Medical Testing***

You’ve made the decision to become a surrogate, have been matched with IPS, and signed the contract. The first medical step is infectious disease testing. . This testing may be done in your local area. The testing will involve a blood draw and vaginal/cervical swabbing. Clinics will often require an in person visit for this testing as well as a uterine evaluation.

***Mock Cycle or Mock Transfer***

A mock cycle involves going through the medication regiment to ensure your body responds to the treatment.  This is not common. A mock transfer involves inserting a catheter similar to one that will be used for the transfer in order for the physician to determine your cervical shape, etc. This is more common and would be completed at your screening appointment.

***Fertility Treatment***

After you’ve completed your medical testing requirements, we will coordinate with you and the intended parents’ clinic to prepare for the medical procedure.

For Gestational Surrogates, this will mean preparing your uterine lining for the embryo transfer. This involves

	Tracking your menstrual cycles so they are in line with the intended mother’s or ovum donor’s cycles.

	Taking fertility medications, some of which may be daily injections.

	You will be required to travel to the intended parents’ fertility clinic the day before the embryo transfer date and will return one to two days after the embryo transfer occurs.

	You will then be on bed rest for at least 24 hours after the transfer.

For Traditional (AI) Surrogates, you will administer medications, usually via injections, that will improve the quality of eggs produced. This means that:

	You will likely have several appointments in your local area to monitor your hormone levels prior to when you travel to the intended parents’ fertility clinic for the insemination.

	The Artificial Insemination procedure feels similar to a pap smear.

In both cases, you will need to complete a blood pregnancy test at your local doctor approximately two weeks after the insemination/embryo transfer.

***Medications***

The fertility medications you will be taking are all very safe. However, there are some minor side effects that women can experience.

	Estrace: These are synthetic estrogen tablets. The purpose of these tablets is to thicken your endometrial lining in preparation for the embryo transfer.

	Progesterone: This medication can be administered as a vaginal suppository, oral medication, or intra-muscular injection. Progesterone is the natural hormone your body produces to maintain a pregnancy. Following an embryo transfer (or occasionally artificial insemination), doctors prescribe these medications to increase or help support your own progesterone to assist in maintaining an early pregnancy.

For more information about medications involved in becoming a surrogate, please visit [ivf centre in delhi](https://www.elawoman.com/blog/in-vitro-fertilisation-ivf/best-ivf-centre-in-delhi-high-success-ratessost)

***Pregnancy and Delivery!***

All of your prenatal appointments will happen in your local area, so the only travel required will be for the actual insemination/embryo transfer. Even after a pregnancy is achieved, your IARC® Program Coordinator will stay in regular contact with you to make sure that everything is going smoothly with your program, that you are being reimbursed for program-related expenses, and that contact between you and the intended parents is going well.

***Bottom Line***

We are here to make sure that as many administrative responsibilities as possible are taken off of your shoulders so you and the intended parents can enjoy the pregnancy and the relationship that will develop between you!  This will be an incredible experience for everyone, and we truly appreciate your willingness to give this amazing gift to another family; it is truly remarkable!

